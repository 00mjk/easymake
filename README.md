# easymake

Simple, lightweight, FOSS alternative to GNU Make or CMake

## Installation

Download the latest version from the releases tab, or build from source (check COMPILING_EASYMAKE file).

## Usage

`$ easymake YOUR_BUILD_FILE`

## Contributing

1. Fork the project
2. Make your changes
3. Remove trailing whitespace (regex `[^\S\r\n]+$`) in files that you modified
2. Create your feature branch: `git checkout -b my-new-feature`
3. Commit your changes: `git commit -am 'Add some feature'`
4. Push to the branch: `git push origin my-new-feature`
5. Submit a pull request

## Credits

[undersquire](https://gitea.com/undersquire) - Original idea, programming, project lead

[DynamicDonkey](https://gitea.com/DynamicDonkey) - JSON buildscript example, README, website design, documentation

[MousieDev](https://gitea.com/MousieDev) - JSON parsing, Bug squasher

## License

[GPL v3](https://gitea.com/undersquire/easymake/raw/branch/main/LICENSE)
